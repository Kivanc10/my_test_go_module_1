package foo

import (
	"fmt"
	"sort"
)

type SortToolsFoo interface {
	Len() int
	Less(i, j int) bool
	Swap(i, j int)
}

type PersonFoo struct {
	Name     string
	LastName string
	Age      int
}

type FooPersonArr []PersonFoo

func (pF FooPersonArr) Len() int {
	return len(pF)
}

func (pF FooPersonArr) Less(i, j int) bool {
	return pF[i].Age < pF[j].Age
}

func (pF FooPersonArr) Swap(i, j int) {
	temp := pF[i]
	pF[i] = pF[j]
	pF[j] = temp
}

func FooTest() {
	people := []PersonFoo{
		{"Bob", "Marley", 31},
		{"John", "Doe", 42},
		{"Michael", "Carrick", 17},
		{"Jenny", "Doyle", 26},
	}
	fmt.Println("before sort -> ", people)
	sort.Sort(FooPersonArr(people))
	fmt.Println("\nAfter sort --->(by age) ", people)
}
