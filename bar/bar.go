package bar

import (
	"errors"
	"fmt"
)

type BarErr struct {
	Arg   int
	Bar_s string
}

func (be *BarErr) Error() string {
	return fmt.Sprintf("%d / %s ", be.Arg, be.Bar_s)
}

func (be *BarErr) New(s string) error {
	return errors.New(s)
}

func Test_One(arg int) (int, error) {
	if arg == 40 {
		return 40, nil
	} else {
		return -1, &BarErr{arg, "we cannot work with this number"}
	}
}

func TestTwo(arg int) (int, error) {
	if arg == 40 {
		return 40, nil
	} else {
		return -1, errors.New("we cannot work with this number v2")
	}
}
