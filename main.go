package main

import (
	"fmt"

	"gitlab.com/Kivanc10/my_test_go_module_1/bar"
	"gitlab.com/Kivanc10/my_test_go_module_1/foo"
)

func main() {
	foo.FooTest()
	if _, e1 := bar.Test_One(41); e1 != nil {
		fmt.Println(e1)
		if serr, ok := e1.(*bar.BarErr); ok {
			fmt.Println("serr --> ", serr)
		}
	} else {
		fmt.Println("this num is compatible for us")
	}

}
